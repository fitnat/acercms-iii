﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AcerCMS.Models;

namespace AcerCMS.ModelService
{
    public class CommentModelService
    {
        public static Comment CreateCommentViewModel(int id, EntityType entity)
        {
            return new Comment()
            {                
                EntityId = id,
                Entity = entity.ToString()
            };
        }

        public static void Save(Comment comment)
        {
            comment.UpdateDate= DateTime.Now;
            
            using (var db = new AcerCmsEntities())
            {
                //throw new Exception("Boooom!");
                db.Comments.Add(comment);
                db.SaveChanges();
            }
        }

        public static CommentViewModel GetComments(int entityId, EntityType entity)
        {
            using (var db = new AcerCmsEntities())
            {
                return new CommentViewModel()
                {
                    Comments = db.Comments.Where(x => x.EntityId == entityId && x.Entity == entity.ToString()).ToList(),
                    EntityId = entityId,
                    EntityType = entity
                };
            }
        }
    }
}